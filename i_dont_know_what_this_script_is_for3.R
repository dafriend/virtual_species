library(rgdal)

col_roads = raster(paste0(PROB_SURFACES_DIR, "/col_roads_distance_1000.grd"))
col_roads = projectRaster(col_roads, crs =CRS(proj4string(vs$pa.raster)))
plot(col_roads)

vs_name = "vs1"
load(paste0(VIRTUAL_SPECIES_DIR, "/",vs_name, ".Rdata"))
#plot(vs)

pres_pts = rasterToPoints(vs$pa.raster, function(x){x == 1}, spatial=TRUE)
abs_pts = rasterToPoints(vs$pa.raster, function(x){x == 0}, spatial=TRUE)

pres_pts_train = crop(pres_pts, vs_train_ext)
pres_pts_val = crop(pres_pts, vs_val_ext)

abs_pts_val = crop(abs_pts, vs_val_ext)

prob_input_raster = raster(extent(pres_pts_train), crs=CRS(proj4string(pres_pts_train)), resolution=res(vs$pa.raster))

prob_surface = get_prob_surface(prob_input_raster, 
                                "distance", 
                                PROB_SURFACES_DIR, 
                                paste0(vs_name, "_train"), 
                                features=col_roads,
                                overwrite=TRUE)
# plot(prob_surface)
rasterToPoints(prob_input_raster, spatial=TRUE)
print("Sampling points")
sample_pts = sample_pts_bias(pres_pts_train, col_roads^15, 5000)
plot(col_roads)
plot(sample_pts, add=TRUE)
plot(grd, add=TRUE)

grd = create_grid(pres_pts, 5000)

grd = create_grid(sample_pts, 10000)

start_time = Sys.time()
grd_old = create_grid(sample_pts, 10000)
test_pts_old = grid_sample_points_old(sample_pts, grd_old, 1)
end_time = Sys.time()
end_time-start_time
test_pts_old

start_time = Sys.time()
grd_new = create_grid(sample_pts, 5000)
test_pts_new = grid_sample_points(sample_pts, grd_new, 1)
end_time = Sys.time()
end_time-start_time
test_pts_new

plot(test_pts_old)
plot(test_pts_new, col="red", pch=1, add=TRUE)

grid_sample_points_old = function(pts, grd, n_per_cell, random=FALSE){
   pts_copy = pts
   pts_copy$grid_sample_id = 1:length(pts_copy)
   return_ids = c()
   #grd = create_grid(pts_copy, side_length, type="polygons")
   sample_ids_raw = lapply(1:length(grd), function(i){
      #cell_ext = extent(grd[i,])
      #get the indices of the points that fall in this square - this is faster than doing 'over'
      #subset_indices = (pts_copy@coords[,1] >= cell_ext[1] & pts_copy@coords[,1] < cell_ext[2]) &
      #(pts_copy@coords[,2] >= cell_ext[3] & pts_copy@coords[,2] < cell_ext[4])
      #pts_i = pts_copy[subset_indices,]
      
      is_in_cell = over(pts_copy, grd[i,])
      pts_i = pts_copy[!is.na(is_in_cell$id),]
      
      if(length(pts_i) == 0){
         return_ids = NULL
      } else if (length(pts_i) < n_per_cell){
         return_ids = pts_i$grid_sample_id
      } else {
         if(random){
            return_ids = sample(pts_i$grid_sample_id, n_per_cell, replace=FALSE)
         } else {
            nn = nndist(pts_i@coords, k=1)
            pts_ids_ordered = pts_i$grid_sample_id[order(nn, decreasing = TRUE)]
            return_ids = pts_ids_ordered[1:n_per_cell]
         }
      }
      return(return_ids)
   })
   sample_ids = unlist(sample_ids_raw)
   return(pts_copy[pts_copy$grid_sample_id %in% sample_ids,])
}

grid_sample_points_new = function(pts, grd, n_per_cell, random=FALSE){
   pts_copy = pts
   pts_copy$grid_sample_id = 1:length(pts_copy)
   return_ids = c()
   #grd = create_grid(pts_copy, side_length, type="polygons")
   sample_ids_raw = lapply(1:length(grd), function(i){
      cell_ext = extent(grd[i,])
      #get the indices of the points that fall in this square - this is faster than doing 'over'
      subset_indices = (pts_copy@coords[,1] >= cell_ext[1] & pts_copy@coords[,1] <= cell_ext[2]) &
                       (pts_copy@coords[,2] >= cell_ext[3] & pts_copy@coords[,2] <= cell_ext[4])
      pts_i = pts_copy[subset_indices,]
      
      #is_in_cell = over(pts_copy, grd[i,])
      #pts_i = pts_copy[!is.na(is_in_cell$id),]
      
      if(length(pts_i) == 0){
         return_ids = NULL
      } else if (length(pts_i) < n_per_cell){
         return_ids = pts_i$grid_sample_id
      } else {
         if(random){
            return_ids = sample(pts_i$grid_sample_id, n_per_cell, replace=FALSE)
         } else {
            nn = nndist(pts_i@coords, k=1)
            pts_ids_ordered = pts_i$grid_sample_id[order(nn, decreasing = TRUE)]
            return_ids = pts_ids_ordered[1:n_per_cell]
         }
      }
      return(return_ids)
   })
   sample_ids = unlist(sample_ids_raw)
   return(pts_copy[pts_copy$grid_sample_id %in% sample_ids,])
}





plot(vs$suitab.raster)
plot(sample_pts, add=TRUE)
plot(sample_pts_cor, add=TRUE)






