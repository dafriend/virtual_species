
vs_name = "vs1"

sample_size = 1000
sample_grid_size = 10000
train_pct = .7
num_background = 10000

load(paste0(VIRTUAL_SPECIES_DIR, "/",vs_name, ".Rdata"))

col_roads = readOGR("data/roads/colorado_roads/tl_2015_08_prisecroads.shp")
col_roads = spTransform(col_roads, CRS(proj4string(input_raster)))
please_work = get_prob_surface(input_raster, 
                               type="distance", 
                               dir=PROB_SURFACES_DIR, 
                               file_name_base = "col_roads", 
                               features = col_roads)

plot(vs1)

pres_pts = rasterToPoints(vs1$pa.raster, function(x){x ==1}, spatial=TRUE)
plot(pres_pts)


unbiased_pts = sample_pts_unbiased(pres_pts, sample_size)
biased_pts = sample_pts_center_bias(vs1$pa.raster,sample_size, bias_exp=5)

plot(vs1_region)
plot(unbiased_pts, add=TRUE)
plot(biased_pts, add=TRUE, col="red")

test = sample_pts(pres_pts, 5000, "none")
plot(test)

test2 = sample_pts(pres_pts, 5000, "center", bias_exp = 10)
sample_grid = create_grid(unbiased_pts, sample_grid_size)
plot(sample_grid)

unbiased_pts_cor = grid_sample_points(unbiased_pts, sample_grid, 1)
biased_pts_cor = grid_sample_points(biased_pts, sample_grid, 1)

unbiased_pts
unbiased_pts_cor
biased_pts_cor

plot(unbiased_pts_cor)
plot(biased_pts_cor)

random_bg = spsample(vs1_region, num_background, type="random")
random_bg_indices = sample(1:length(random_bg), floor(.7*length(random_bg)), replace=FALSE)
random_bg_train = random_bg[random_bg_indices,]
random_bg_val = random_bg[-random_bg_indices,]
plot(random_bg_train)
plot(random_bg_val)

biased_pts_dat = data_split(biased_pts, train_pct)
unbiased_pts_dat = data_split(unbiased_pts, train_pct)
biased_pts_cor_dat = data_split(biased_pts_cor, train_pct)
unbiased_pts_cor_dat = data_split(unbiased_pts_cor, train_pct)


plot(vs1$suitab.raster)
biased_me = maxent(vs1_vars, biased_pts_dat$train, a=random_bg_train)
biased_me_pred = predict(biased_me, vs1_vars)
biased_me_eval = evaluate(biased_pts_dat$val, random_bg_val, biased_me, vs1_vars)
plot(biased_me_pred)
biased_me_eval

unbiased_me = maxent(vs1_vars, unbiased_pts_dat$train, a=random_bg_train)
unbiased_me_pred = predict(unbiased_me, vs1_vars)
unbiased_me_eval = evaluate(unbiased_pts_dat$val, random_bg_val, unbiased_me, vs1_vars)
plot(unbiased_me_pred)
unbiased_me_eval

biased_cor_me = maxent(vs1_vars, biased_pts_cor_dat$train, a=random_bg_train)
biased_cor_me_pred = predict(biased_cor_me, vs1_vars)
biased_cor_me_eval = evaluate(biased_pts_cor_dat$val, random_bg_val, biased_cor_me, vs1_vars)
plot(biased_cor_me_pred)
biased_cor_me_eval

unbiased_cor_me = maxent(vs1_vars, unbiased_pts_cor_dat$train, a=random_bg_train)
unbiased_cor_me_pred = predict(unbiased_cor_me, vs1_vars)
unbiased_cor_me_eval = evaluate(unbiased_pts_cor_dat$val, random_bg_val, unbiased_cor_me, vs1_vars)
plot(unbiased_cor_me_pred)
unbiased_me_eval

truth = vs1$suitab.raster
sqrt(cellStats((truth-biased_me_pred)^2, stat="mean"))
sqrt(cellStats((truth-unbiased_me_pred)^2, stat="mean"))
sqrt(cellStats((truth-biased_cor_me_pred)^2, stat="mean"))
sqrt(cellStats((truth-unbiased_cor_me_pred)^2, stat="mean"))
