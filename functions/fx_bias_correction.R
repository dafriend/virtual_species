maxent_eval2 = function(pres_pts, abs_pts, pred_vars, actual_pa, actual_suitability, stats_n_iter = 10, stats_sample_size=250){
   #print("Preparing data")
   pres_pts_all = rasterToPoints(actual_pa, function(x){ x == 1 }, spatial=TRUE)
   abs_pts_all = rasterToPoints(actual_pa, function(x){ x == 0 }, spatial=TRUE)
   
   #print("Fitting model")
   me = maxent(pred_vars, pres_pts, a=abs_pts)
   
   #print("Making predictions")
   me_pred = predict(me, pred_vars)
   
   stats_all = calc_stats(me_pred, actual_suitability, n_iter = stats_n_iter, sample_size = stats_sample_size)
   
   colnames(stats_all) = paste0(colnames(stats_all), "_all")
   
   #print("Calculating basic statistics")
   #actual_sample_size = length(pres_pts)
   
   results = cbind(actual_sample_size = length(pres_pts), 
                   auc__model = raster_auc(pres_pts, abs_pts, me_pred),
                   auc_all = raster_auc(pres_pts_all, abs_pts_all, me_pred),
                   stats_all)
   results = results[,order(colnames(results))] #alphabetize the columns - this makes it more readable in the ouput csv
   
   # results = cbind(
   #    actual_sample_size = actual_sample_size,
   #    model_auc = me_eval@auc,
   #    auc_all = me_eval_all@auc,
   #    auc_train = me_eval_train@auc,
   #    auc_val = me_eval_val@auc,
   #    rmse_all = rmse_all,
   #    rmse_train = rmse_train,
   #    rmse_val = rmse_val,
   #    cor_all = cor_all,
   #    cor_train = cor_train,
   #    cor_val = cor_val,
   #    rsq_all = r2_all,
   #    rsq_train = r2_train,
   #    rsq_val = r2_val)
   return(list(model = me, pred = me_pred, stats = results))
}


maxent_eval = function(pres_pts, abs_pts, pred_vars, train_ext, val_ext, actual_pa, actual_suitability, stats_n_iter = 10, stats_sample_size=250){
   #print("Preparing data")
   pres_pts_all = rasterToPoints(actual_pa, function(x){ x == 1 }, spatial=TRUE)
   pres_pts_train = crop(pres_pts_all, train_ext)
   pres_pts_val = crop(pres_pts_all, val_ext)
   
   abs_pts_all = rasterToPoints(actual_pa, function(x){ x == 0 }, spatial=TRUE)
   abs_pts_train = crop(abs_pts_all, train_ext)
   abs_pts_val = crop(abs_pts_all, val_ext)
   
   #print("Fitting model")
   me = maxent(pred_vars, pres_pts, a=abs_pts)
   
   #print("Making predictions")
   me_pred = predict(me, pred_vars)
   
   #print("Evaluating predictions")
   actual_suit_train = crop(actual_suitability, train_ext)
   actual_suit_val = crop(actual_suitability, val_ext)
   me_pred_train = crop(me_pred, train_ext)
   me_pred_val = crop(me_pred, val_ext)
   
   #me_eval = evaluate(pres_pts, abs_pts, me, pred_vars)
   #me_eval_all = evaluate(pres_pts_all, abs_pts_all, me, pred_vars)
   #me_eval_train = evaluate(pres_pts_train, abs_pts_train, me, pred_vars)
   #me_eval_val = evaluate(pres_pts_val, abs_pts_val, me, pred_vars)
   
   #print("Calculating RMSE and correlation")
   #rmse_all = raster_rmse(me_pred, actual_suitability)
   #rmse_train = raster_rmse(me_pred_train, actual_suit_train)
   #rmse_val = raster_rmse(me_pred_val, actual_suit_val)
   
   #cor_all = cor(as.vector(me_pred), as.vector(actual_suitability), use="pairwise.complete.obs")
   #cor_train = cor(as.vector(me_pred_train), as.vector(actual_suit_train), use="pairwise.complete.obs")
   #cor_val = cor(as.vector(me_pred_val), as.vector(actual_suit_val), use="pairwise.complete.obs")
   
   #--- R^2
   #r2_all = raster_r2(me_pred, actual_suitability)
   #r2_train = raster_r2(me_pred_train, actual_suit_train)
   #r2_val = raster_r2(me_pred_val, actual_suit_val)
   
   stats_all = calc_stats(me_pred, actual_suitability, n_iter = stats_n_iter, sample_size = stats_sample_size)
   stats_train = calc_stats(me_pred_train, actual_suit_train, n_iter = stats_n_iter, sample_size = stats_sample_size)
   stats_val = calc_stats(me_pred_val, actual_suit_val, n_iter = stats_n_iter, sample_size = stats_sample_size)
   
   colnames(stats_all) = paste0(colnames(stats_all), "_all")
   colnames(stats_train) = paste0(colnames(stats_train), "_train")
   colnames(stats_val) = paste0(colnames(stats_val), "_val")
   
   stats_comb = cbind(stats_all, stats_train, stats_val)
   #print("Calculating basic statistics")
   actual_sample_size = length(pres_pts)
   
   results = cbind(actual_sample_size = length(pres_pts), 
                   auc__model = raster_auc(pres_pts, abs_pts, me_pred),
                   auc_all = raster_auc(pres_pts_all, abs_pts_all, me_pred),
                   auc_train = raster_auc(pres_pts_train, abs_pts_train, me_pred),
                   auc_val = raster_auc(pres_pts_val, abs_pts_val, me_pred),
                   stats_comb)
   results = results[,order(colnames(results))] #alphabetize the columns - this makes it more readable in the ouput csv
   
   # results = cbind(
   #    actual_sample_size = actual_sample_size,
   #    model_auc = me_eval@auc,
   #    auc_all = me_eval_all@auc,
   #    auc_train = me_eval_train@auc,
   #    auc_val = me_eval_val@auc,
   #    rmse_all = rmse_all,
   #    rmse_train = rmse_train,
   #    rmse_val = rmse_val,
   #    cor_all = cor_all,
   #    cor_train = cor_train,
   #    cor_val = cor_val,
   #    rsq_all = r2_all,
   #    rsq_train = r2_train,
   #    rsq_val = r2_val)
   return(list(model = me, pred = me_pred, stats = results))
}

calc_stats = function(pred, actual, n_iter=10, sample_size = 250){
   rmserr = raster_rmse(pred, actual)
   corr = cor(as.vector(pred), as.vector(actual), use="pairwise.complete.obs")
   rsq = raster_r2(pred, actual)
   
   pred_vec = as.vector(pred)
   actual_vec = as.vector(actual)
   
   iter_stats = sapply(1:n_iter, function(i){
      rand_indices = sample(1:length(pred_vec), sample_size, replace=FALSE)
      pred_i = pred_vec[rand_indices]
      actual_i = actual_vec[rand_indices]
      result = cbind(rsq = r2(pred_i, actual_i, na.rm=TRUE),
                     rmse = rmse(pred_i, actual_i, na.rm=TRUE),
                     cor = cor(pred_i, actual_i, use="pairwise.complete.obs"))
      return(result)
   })
   mean_iter_stats = apply(t(iter_stats), MARGIN=2, mean)
   names(mean_iter_stats) = c("rsq", "rmse", "cor")
   
   return(cbind(rmse = rmserr,
                cor = corr,
                rsq = rsq,
                rmse__iter = mean_iter_stats["rmse"],
                cor__iter = mean_iter_stats["cor"],
                rsq__iter = mean_iter_stats["rsq"]))
}

raster_auc = function(pres_pts,abs_pts, pred_raster){
   pres_vals = extract(pred_raster, pres_pts)
   abs_vals = extract(pred_raster, abs_pts)
   comb_mat = rbind(cbind(pres_vals, 1), cbind(abs_vals, 0))
   result = auc(comb_mat[,2], comb_mat[,1]) #from the pROC package
   return(as.numeric(result))
}

r2 = function(pred, actual, na.rm=FALSE){
    result = 1-sum((actual - pred)^2, na.rm = na.rm)/sum((actual - mean(actual, na.rm = na.rm))^2, na.rm = na.rm)
    return(result)
}

rmse = function(pred, actual, na.rm=FALSE){
   result = sqrt(mean((actual - pred)^2, na.rm = na.rm))
   return(result)
}

raster_r2 = function(pred, actual){
   actual_mean = cellStats(actual, "mean")
   r2 = 1-cellStats((actual - pred)^2, "sum")/cellStats((actual - actual_mean)^2, "sum")
   return(r2)
}

raster_rmse = function(pred,  actual){
   return(sqrt(cellStats((actual-pred)^2, stat="mean")))
}
correct_bias = function(pts, type, ...){
   if(type == "grid"){
      return(correct_bias_grid(pts, ...))
   }
}

correct_bias_grid = function(pts, side_length, n_per_cell){
   sample_grid = create_grid(pts, side_length)
   pts_cor = grid_sample_points(pts, sample_grid, n_per_cell)
   return(pts_cor)
}
#----------------------------
#create_grid()
#----------------------------
#function for creating a grid over a set of points
#PARAMETERS:
# -> pts - a SpatialPoints* object
# -> side_length - a number that specifies the side length for the grid cells. In the same units as 'pts'
# -> type - either "polygons" or "points". "polygons" returns a grid of square
# polygons, while "points" returns a set of points arranged in a grid
#RETURNS:
# -> A SpatialPolygonsDataFrame or SpatialPoints object representing the grid
create_grid = function(pts, side_length, type="polygons"){
   if(!(type %in% c("polygons", "points"))){
      stop("'type' must be either 'polygons' or 'points'")
   }
   bb = bbox(pts) #get the xmin and xmax
   xgrid = seq(from=bb[1,1], by=side_length, length.out=ceiling((bb[1,2]-bb[1,1])/side_length)+1) #calculate the locations for the vertical grid lines. The code for length.out ensures that the grid will cover the entire area and won't leave any points out at the end
   ygrid = seq(from=bb[2,1], by=side_length, length.out=ceiling((bb[2,2]-bb[2,1])/side_length)+1) #calculate the locations for the horizontal grid lines.
   xypts_mat = expand.grid(xgrid, ygrid) #this gets a matrix that represents the coordinates of all places where the grid lines will cross
   xypts = SpatialPoints(xypts_mat, proj4string = CRS(proj4string(pts))) #convert that to be a SpatialPoints object
   if(type == "points"){
      return(xypts)
   } else if (type == "polygons"){
      gridded(xypts) = TRUE #don't actually know what this does... copied it from some of Ken's code
      xygrid = as(xypts, "SpatialPolygons") #convert the points to polygons... again I'm not entirely sure how it's performing the conversion - copied this from some of Ken's code
      xygrid_df = SpatialPolygonsDataFrame(xygrid, data=data.frame(id=1:length(xygrid), row.names=names(xygrid)))#convert the SpatialPolygons to a SpatialPolygonsDataFrame
      return(xygrid_df)
   }
}


grid_sample_points = function(pts, grd, n_per_cell, random=FALSE){
   pts_copy = pts
   pts_copy$grid_sample_id = 1:length(pts_copy)
   return_ids = c()
   #grd = create_grid(pts_copy, side_length, type="polygons")
   sample_ids_raw = lapply(1:length(grd), function(i){
      cell_ext = extent(grd[i,])
      #get the indices of the points that fall in this square - this is MUCH faster than doing 'over' - I ran a trial and this took 1.5 seconds while 'over' took 29 seconds!
      subset_indices = (pts_copy@coords[,1] >= cell_ext[1] & pts_copy@coords[,1] < cell_ext[2]) &
                       (pts_copy@coords[,2] >= cell_ext[3] & pts_copy@coords[,2] < cell_ext[4])
      pts_i = pts_copy[subset_indices,]
      
      #is_in_cell = over(pts_copy, grd[i,])
      #pts_i = pts_copy[!is.na(is_in_cell$id),]
      
      if(length(pts_i) == 0){
         return_ids = NULL
      } else if (length(pts_i) < n_per_cell){
         return_ids = pts_i$grid_sample_id
      } else {
         if(random){
            return_ids = sample(pts_i$grid_sample_id, n_per_cell, replace=FALSE)
         } else {
            nn = nndist(pts_i@coords, k=1)
            pts_ids_ordered = pts_i$grid_sample_id[order(nn, decreasing = TRUE)]
            return_ids = pts_ids_ordered[1:n_per_cell]
         }
      }
      return(return_ids)
   })
   sample_ids = unlist(sample_ids_raw)
   return(pts_copy[pts_copy$grid_sample_id %in% sample_ids,])
}

restricted_background = function(pts, sample_size, buffer_length){
   buffered = raster::buffer(pts, buffer_length)
   sample_pts = spsample(buffered, sample_size, type="random")
   return(sample_pts)
}
