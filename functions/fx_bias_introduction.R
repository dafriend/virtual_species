
library(rgeos)
library(raster)
library(sp)

#add_bias_gradient = function(pts){
#   
#}

get_prob_surface = function(input_raster, type, dir, file_name_base, overwrite=FALSE, ...){
   file_name = paste0(dir,"/",file_name_base, "_", type, "_", res(input_raster)[1], ".grd")
   if(file.exists(file_name) & !overwrite){
      #print("file exists")
      return(raster(file_name))
   } else {
      #print("creating raster")
      prob_surface = create_prob_surface(input_raster, type, ...)
      writeRaster(prob_surface, file_name, overwrite=overwrite)
      return(prob_surface)
   }
}

#wrapper function for the three 'create_prob_surface_*' functions.
create_prob_surface = function(input_raster, type, pts=NULL, features=NULL){
   if(type == "uniform"){
      return(create_prob_surface_uniform(input_raster))
   } else if (type == "center"){
      return(create_prob_surface_center(input_raster, pts))
   } else if (type == "distance"){
      return(create_prob_surface_distance(input_raster, features))
   }
}

#creates a raster where every cell contains 1
#PARAMETERS:
   #input_raster - A 'raster' object. Each cell will be populated with
      #a 1 and then this raster will be returned
#RETURNS:
   #a 'raster' object the same as 'input_raster' but with every cell having a
   #value of 1.
create_prob_surface_uniform = function(input_raster){
   input_raster[] = 1
   return(input_raster)
}

#creates a raster with values between 0 and 1 where the value of each cell is
#determined by its distance the centroid of a given set of points.
#PARAMETERS: 
   #input_raster - A 'raster' object. Each cell will be populated with number
      #between 0 and 1 representing the relative distance to the centroid of
      #'pts'
   #pts - a 'SpatialPoints*' object
#RETURNS:
   #A 'raster' object with values between 0 and 1, with the value being higher
   #the closer the cell is to the centroid of 'pts'
create_prob_surface_center = function(input_raster, pts){
   centroid_raw = apply(pts@coords, MARGIN=2, mean)
   centroid = SpatialPoints(rbind(centroid_raw), proj4string=CRS(proj4string(pts)))
   return(create_prob_surface_distance(input_raster, centroid))
}

#creates a raster with values between 0 and 1 where the value of each cell is
#determined by its minimum distance to a feature provided by the user.
#PARAMETERS:
   #input_raster - A 'raster' object. Each cell will be populated with number
      #between 0 and 1 representing the relative (shortest) distance to 
      #'features'
   #features - A Spatial* object 
#RETURNS:
   #A 'raster' object.
create_prob_surface_distance = function(input_raster, features){
   dists_raw = gDistance(features, rasterToPoints(input_raster, spatial=TRUE), byid=TRUE)
   input_raster[] = apply(dists_raw,1,min)
   prob_raster = convert_dist_to_prob(input_raster)
   return(prob_raster)
}
# 
# get_prob_surface_center = function(){
#    
# }
# 
# get_prob_surface_distance = function(){
#    
# }

sample_pts = function(pts, sample_size, bias_type, bias_exp=NULL, distance_feature=NULL, resolution=NULL){
   if(bias_type == "none"){
      return(sample_pts_unbiased(pts, sample_size))
   } else if (bias_type == "center"){
      return(sample_pts_center_bias(pts, sample_size, bias_exp, resolution))
   } else if (bias_type == "distance"){
      return(sample_pts_distance_bias(pts, sample_size, distance_feature, bias_exp, resolution))
   }
}

# add_bias_center = function(pts, resolution){
#    centroid_raw = apply(pts@coords, MARGIN=2, mean)
#    centroid = SpatialPoints(rbind(centroid_raw), proj4string=CRS(proj4string(pts)))
#    
#    dist_raster = raster(extent(pts), crs=CRS(proj4string(pts)), resolution=resolution)
#    dists_raw = gDistance(centroid, as(dist_raster, "SpatialPoints"), byid=TRUE)
#    
#    
#    plot(dist_to_centroid   )
#    dist_raster[] = apply(dist_to_centroid,1,min)
#    
#    plot(dist_raster)
#    points(centroid)
#    
#    max_dist = max(dist_raster)
#    
# }

#sample_pts_feature_bias = function(pa_raster, feature, sample_size, bias_exp=1)

#given a presence-absence raster, creates a biased data set with the centroid of
#the distribution more likely to be sampled
#PARAMETERS:
   #pa_raster: a raster of 0s and 1s, where 1 represents presence and 0
      #represents absence. The output from the virtualspecies package can be put
      #in here (the "pa.raster" element of the output)
   #sample_size: integer - the number of points to sample
   #bias_exp: numeric - the "level" of bias. 0 will result in no bias. Bias
      #increases as this value increases
#RETURNS:
   #A SpatialPoints object with 'sample_size' points. 
# sample_pts_center_bias = function(pa_raster, sample_size, bias_exp=1){
#    #get points for each of the "presence" cells in the raster - these are all of the possible presence points
#    pts = rasterToPoints(pa_raster, function(x){x == 1}, spatial=TRUE )
#    #get the centroid of all of the presence cells
#    centroid_raw = apply(pts@coords, MARGIN=2, mean)
#    centroid = SpatialPoints(rbind(centroid_raw), proj4string=CRS(proj4string(pts)))
#    
#    return(sample_pts_distance_bias(pa_raster, centroid, sample_size, bias_exp))
# }

sample_pts_center_bias = function(pts, sample_size, bias_exp=1){
   #get the centroid of all of the presence cells
   centroid_raw = apply(pts@coords, MARGIN=2, mean)
   centroid = SpatialPoints(rbind(centroid_raw), proj4string=CRS(proj4string(pts)))
   
   return(sample_pts_distance_bias(pts, centroid, sample_size, bias_exp))
}

# 
# sample_pts_distance_bias = function(pa_raster, bias_features, sample_size, bias_exp=1){
#    #get points for each of the "presence" cells in the raster - these are all of the possible presence points
#    pts = rasterToPoints(pa_raster, function(x){x == 1}, spatial=TRUE )
#    
#    #now we'll create a raster where each cell contains the distance to the centroid
#    dist_raster = raster(extent(pts), crs=CRS(proj4string(pts)), resolution=res(pa_raster))
#    dists_raw = gDistance(bias_features, as(dist_raster, "SpatialPoints"), byid=TRUE)
#    dist_raster[] = apply(dists_raw,1,min)
#    #dist_raster[] = dists_raw
#    #plot(dist_raster)
#    #convert the distances to a value between 0 and 1
#    max_dist = cellStats(dist_raster, stat="max")
#    min_dist = cellStats(dist_raster, stat="min")
#    prob_raster = ((max_dist - dist_raster)/(max_dist-min_dist))
#    #plot(prob_raster)
#    #get the probabiliy at each point's location
#    probs = extract(prob_raster, pts)
#    
#    #sample the indices, using 'prob_raster' as the probabilities. The probabilities are raised to the 'bias_exp' power to allow us to control the "severity" of the bias. As 'bias_exp' increases the disparity between high and low values will increase, which wil increase the degree of bias in the sampled data set
#    sample_indices = sample(1:length(pts), sample_size, replace=FALSE, prob = probs^bias_exp)
#    sample_pts = pts[sample_indices,]
#    #plot(pa_raster)
#    #plot(bias_features, add=TRUE)
#    #plot(sample_pts, add=TRUE)
#    return(sample_pts)
# }


sample_pts_distance_bias = function(pts, sample_size, bias_features, bias_exp=1){
   #now we'll create a raster where each cell contains the distance to the centroid
   input_raster = raster(extent(pts), crs=CRS(proj4string(pts)), resolution=res(pa_raster))
   dist_raster = get_raster_distance_from(bias_features, input_raster)
   
   prob_raster = convert_dist_to_prob(dist_raster)
   sample_pts = sample_pts_bias(pts, prob_raster^bias_exp, sample_size)
   
   return(sample_pts)
}

# sample_pts_distance_bias = function(pa_raster, sample_size, bias_features, bias_exp=1){
#    #get points for each of the "presence" cells in the raster - these are all of the possible presence points
#    pts = rasterToPoints(pa_raster, function(x){x == 1}, spatial=TRUE )
#    
#    #now we'll create a raster where each cell contains the distance to the centroid
#    input_raster = raster(extent(pts), crs=CRS(proj4string(pts)), resolution=res(pa_raster))
#    dist_raster = get_raster_distance_from(bias_features, input_raster)
#    
#    prob_raster = convert_dist_to_prob(dist_raster)
#    sample_pts = sample_pts_bias(pts, prob_raster^bias_exp, sample_size)
#    
#    return(sample_pts)
#    #max_dist = cellStats(dist_raster, stat="max")
#    #min_dist = cellStats(dist_raster, stat="min")
#    #prob_raster = ((max_dist - dist_raster)/(max_dist-min_dist))
#    #plot(prob_raster)
#    #get the probabiliy at each point's location
#    #probs = extract(prob_raster, pts)
#    
#    #sample the indices, using 'prob_raster' as the probabilities. The probabilities are raised to the 'bias_exp' power to allow us to control the "severity" of the bias. As 'bias_exp' increases the disparity between high and low values will increase, which wil increase the degree of bias in the sampled data set
#    #sample_indices = sample(1:length(pts), sample_size, replace=FALSE, prob = probs^bias_exp)
#    #sample_pts = pts[sample_indices,]
#    #plot(pa_raster)
#    #plot(bias_features, add=TRUE)
#    #plot(sample_pts, add=TRUE)
#    #return(sample_pts)
# }

#create a raster where the value of each cell is the distance to a given feature
get_raster_distance_from = function(features, input_raster){
   #dist_raster = raster(output_extent, crs=CRS(proj4string(p)), resolution=resolution)
   dists_raw = gDistance(features, as(input_raster, "SpatialPoints"), byid=TRUE)
   input_raster[] = apply(dists_raw,1,min)
   return(input_raster)
}

#adjusts the values of the raster cells so that they fall between 0 and 1
convert_dist_to_prob = function(dist_raster){
   max_dist = cellStats(dist_raster, stat="max")
   min_dist = cellStats(dist_raster, stat="min")
   prob_raster = ((max_dist - dist_raster)/(max_dist-min_dist))
   return(prob_raster)
}

#Given a set of points and a raster that represents probability of 
#of selection, samples points from the given set of points
sample_pts_bias = function(pts, prob_raster, sample_size){
   probs = extract(prob_raster, pts)
   sample_indices = sample(1:length(pts), sample_size, replace=FALSE, prob = probs)
   sample_pts = pts[sample_indices,]
}

sample_pts_unbiased = function(pts, sample_size){
   sample_indices = sample(1:length(pts), sample_size, replace=FALSE)
   sample_pts = pts[sample_indices,]
}

data_split = function(pts, train_pct){
   train_indices = sample(1:length(pts), floor(train_pct*length(pts)), replace = FALSE)
   train = pts[train_indices,]
   val = pts[-train_indices,]
   return(list(train = train, val = val))
}

data_split_geo = function(pts, train_ext, val_ext){
   
}
#add_bias_prob_raster = function(){
#   
#}






# 
# 
# 
# set.seed(11)
# vs = generateRandomSp(env_vars, #ADJUSTABLE
#                       approach = "pca", #don't have a good reason for this right now - this is just what worked; ADJUSTABLE
#                       niche.breadth = "wide") #I wanted a smaller area of suitable habitat. But this was an arbitrary decision; ADJUSTABLE
# plot(vs)
# 
# 
# 
# pa = vs$pa.raster
# nv_roads = readOGR("NV_ROADS_Shapefiles/NV_ROADS.shp")
# nv_roads = spTransform(nv_roads, CRS("+init=epsg:26911"))
# 
# pa
# nv_roads
# plot(nv_roads)
# 
# start_time = Sys.time()
# roads_bias = sample_pts_distance_bias(pa, nv_roads, 1000, bias_exp=10)
# end_time = Sys.time()
# end_time - start_time
# plot(roads_bias)
# 
# center_bias = sample_pts_center_bias(pa, 1000, bias_exp=10)
# plot(pa)
# plot(center_bias, add=TRUE)
# pa
# pa_pts = rasterToPoints(pa, function(x){x == 1}, spatial=TRUE )
# 
# pts = pa_pts
# resolution = 1000
# 
# centroid_raw = apply(pts@coords, MARGIN=2, mean)
# centroid = SpatialPoints(rbind(centroid_raw), proj4string=CRS(proj4string(pts)))
# 
# dist_raster = raster(extent(pts), crs=CRS(proj4string(pts)), resolution=resolution)
# dists_raw = gDistance(centroid, as(dist_raster, "SpatialPoints"), byid=TRUE)
# 
# dist_raster[] = dists_raw
# #dist_raster[] = apply(dist_to_centroid,1,min)
# 
# plot(dist_raster)
# points(centroid)
# 
# dist_raster
# max_dist = cellStats(dist_raster, stat="max")
# min_dist = cellStats(dist_raster, stat="min")
# 
# prob_raster = ((max_dist - dist_raster)/(max_dist-min_dist))
# plot(prob_raster)
# 
# probs = extract(prob_raster, pts)
# length(probs)
# 
# length(pts)
# head(probs)
# sample_indices = sample(1:length(pts), 1000, replace=FALSE, prob = probs)
# sample_pts = pts[sample_indices,]
# plot(prob_raster)
# plot(vs$suitab.raster)
# plot(sample_pts)
