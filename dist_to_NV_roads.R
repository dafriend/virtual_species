setwd("C:/Users/Derek Friend/OneDrive/Documents/UNR Spring 2019/GEOG 701M/Project")

library(raster)
library(rgdal)
library(rgeos)
nv_roads = readOGR("NV_ROADS_Shapefiles/NV_ROADS.shp")
plot(nv_roads)

nv_roads = spTransform(nv_roads, CRS("+init=epsg:26911"))
nv_roads

plot(region)




nv_roads2 = readOGR("tl_2013_32_prisecroads/tl_2013_32_prisecroads.shp")
plot(nv_roads2)

nv_roads2 = spTransform(nv_roads2, CRS("+init=epsg:26911"))

nv_roads3 = readOGR("nv_roads/nv_roads.shp")
nv_roads3
plot(nv_roads3)

distRaster = raster(extent(nv_roads2), crs=CRS(proj4string(nv_roads2)))
res(distRaster) = 10000
dist_to_road = gDistance(nv_roads2, as(distRaster, "SpatialPoints"), byid=TRUE)

distRaster[] = apply(dist_to_road,1,min)
plot(distRaster)

test = log(distRaster)
plot(test)
