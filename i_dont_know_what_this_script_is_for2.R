
library(ggplot2)
library(dplyr)
library(plyr)

include_iter_plots = FALSE
jpg_width = 1500
jpg_height = 1500

vs_nums = c(5,7,8)
vs_nums = 8
vs_names = paste0("vs", vs_nums)
df_list = lapply(vs_nums, function(num_i){
   df_i = read.csv(paste0(OUTPUT_DIR, "/results_vs", num_i, ".csv"))
   df_i$vs = paste0("VS", num_i-4)
   return(df_i)
})
big_df = bind_rows(df_list)
big_df$corrected2 = big_df$corrected
big_df$corrected2[big_df$corrected] = "Corrected"
big_df$corrected2[!big_df$corrected] = "Uncorrected"
big_df$corrected2 = as.factor(big_df$corrected2)
#get the columns that contain the output statistics - all of the data columns are next to each other, so if I just give the name of the first one and the last one then I can get all of them without having to type them all out
first_data_col_name = "auc__model"
last_data_col_name = "rsq_all"
#data_col_names = names(big_df)[which(names(big_df) == first_data_col_name):which(names(big_df) == last_data_col_name)] 
#if(!include_iter_plots){
#   iter_plot_bool = grepl("iter", data_col_names)
#   data_col_names = data_col_names[!iter_plot_bool]
#}
data_col_names = c("auc_all", "cor_all", "rsq_all")
nice_names = c("AUC", "Correlation", "R-squared")
#sapply(big_df, function(col_i){
#   return(length(unique(col_i)))
#})


varying_vars = c("bias_type")
#varying_vars = c("sample_size", "bias_type", "grid_side_length")
group_factor = interaction(big_df[,varying_vars])
big_df$group = group_factor
group_levels = levels(big_df$group)

gg_color_hue <- function(n) {
   hues = seq(15, 375, length = n + 1)
   hcl(h = hues, l = 65, c = 100)[1:n]
}

#plot_mat_dir = paste0("/Users/dfriend/Desktop/GEOG_701M_Project/plot_matrices/", vs_name)
sapply(1:length(group_levels), function(i){
   level_i = group_levels[i]
   group_i = big_df[big_df$group == level_i,]
   
   
   color_i = gg_color_hue(4)[vs_nums-4]
   #there should only be one value for each of these
   sample_size_i = unique(group_i$sample_size)
   bias_type_i = unique(group_i$bias_type)
   grid_side_length_i = unique(group_i$grid_side_length)
   
   group_agg = aggregate(. ~ vs + grid_side_length + sample_size + corrected2 + bias_exp, group_i, mean)
   
   comb_dir = "/Users/dfriend/Desktop/GEOG_701M_Project/performance_comb"
   for(j in 1:length(data_col_names)){
      plot_dat = group_agg
      #plot_dat$sample_size = as.factor(plot_dat$sample_size)
      #plot_dat$grid_side_length = as.factor(plot_dat$grid_side_length)
      plot_dat$value = group_agg[,data_col_names[j]]
      #plot_dat = group_agg[,c("vs", "bias_exp", "grid_side_length", "sample_size", "corrected", data_col_names[j])]
      #colnames(plot_dat) = c("vs", "bias_exp", "grid_side_length", "corrected", "value")
      ss_labels = paste0("sample size: ", unique(plot_dat$sample_size))
      names(ss_labels) = c(500, 1000, 5000)
      gsl_labels = paste0("grid side length: ", unique(plot_dat$grid_side_length), "m")
      names(gsl_labels) = c(5000,10000,20000)
      bias_type_names = c(center = "core points", distance="roads")
      
      plot_i = ggplot(plot_dat, aes(x=bias_exp, y=value)) + 
         geom_line(aes(linetype=corrected2), color= color_i) + 
         geom_point(aes(shape=vs), size=.9, color=color_i) +
         ylab(nice_names[j]) +
         xlab("Bias level") +
         ggtitle(paste0("Virtual Species ", vs_nums-4, " | ",nice_names[j], " vs. bias level | bias type: ", bias_type_names[level_i])) + 
         facet_grid(rows = vars(grid_side_length), cols=vars(sample_size), labeller = labeller(grid_side_length = gsl_labels, sample_size = ss_labels)) + 
         labs(color="", shape="", linetype="")
      plot_i
      ggsave(paste0(comb_dir, "/comb_", paste0(vs_names, collapse="_"),"_bias_type_", bias_type_i, "_stat_", data_col_names[j],".jpg"), device="jpeg", width=7, height=6)
   }
   #col_name_j = data_col_names[j]
   #plot_dat = group_agg[,c("vs", "bias_exp", "corrected", col_name_j)]
   #colnames(plot_dat) = c("vs", "bias_exp", "corrected", "value")
   #plot_i = ggplot(group_agg, aes(x=bias_exp, y=cor_all)) + 
   #geom_line(aes(color=vs, linetype=corrected)) + 
   #geom_point(aes(color=vs, shape=vs)) +
   #ylab("auc_all") + 
   #ggtitle("auc_all") + 
   #facet_grid(rows = vars(grid_side_length), cols=vars(sample_size))
   #ggsave(paste0(comb_dir))
   #plot_i
   #jpeg(paste0(plot_mat_dir, "/simple_", vs_name, "_samp_", sample_size_i, "_bias_type_", bias_type_i, "_grid_side_length_", grid_side_length_i,".jpg"), width = jpg_width, height=jpg_height)
   
   #n_plots_per_side = ceiling(sqrt(length(data_col_names)))
   #par(mfrow = c(n_plots_per_side,n_plots_per_side), cex.main=2.2, cex.lab = 1.6, cex.axis = 1.6, oma=c(0,2,4,1))
   # plot_list = lapply(1:length(data_col_names), function(j){
   #    col_name_j = data_col_names[j]
   #    plot_dat = group_agg[,c("vs", "bias_exp", "corrected", col_name_j)]
   #    colnames(plot_dat) = c("vs", "bias_exp", "corrected", "value")
   #    plot_i = ggplot(plot_dat, aes(x=bias_exp, y=value)) + geom_line(aes(color=vs, linetype=corrected)) + ylab(col_name_j) + ggtitle(col_name_j)
   #    
   #    ggplot()
   #    
   #    yrange = c(min(group_agg[,col_name_j]), max(group_agg[,col_name_j]))
   #    corrected = group_agg[group_agg$corrected == TRUE,]
   #    uncorrected = group_agg[group_agg$corrected == FALSE,]
   #    plot(uncorrected[,"bias_exp"], uncorrected[,col_name_j], type="l", col="black", lwd=2, main=nice_names[j], ylab=nice_names[j], xlab="Bias level", ylim=yrange)
   #    lines(corrected[,"bias_exp"], corrected[,col_name_j], type="l", col="red", lwd=2)
   #    legend(x="bottomright", legend = c(paste0("uncorrected"), "corrected"), lty=c(1,1), lwd=c(2,2), col=c("black", "red"), inset=c(0,1), xpd=TRUE, bty="n", horiz=FALSE, cex=1.7)
   #    return(NULL)
   # })
   # mtext(paste0("Virtual Species ", vs_num, " | sample size = ", sample_size_i, " | bias type = '", bias_type_i, "' | grid side length = ", grid_side_length_i, "m"), cex=2.0, outer=TRUE)
   # dev.off()
   
})

# corrplot_dir = paste0("/Users/dfriend/Desktop/GEOG_701M_Project/corrplots/", vs_name)
# sapply(1:length(group_levels), function(i){
#    level_i = group_levels[i]
#    group_i = big_df[big_df$group == level_i,]
#    
#    #there should only be one value for each of these
#    sample_size_i = unique(group_i$sample_size)
#    bias_type_i = unique(group_i$bias_type)
#    grid_side_length_i = unique(group_i$grid_side_length)
#    
#    group_agg = aggregate(. ~ bias_exp + corrected, group_i, mean)
#    
#    jpeg(paste0(corrplot_dir, "/", vs_name, "_samp_", sample_size_i, "_bias_type_", bias_type_i, "_grid_side_length_", grid_side_length_i,".jpg"), width = 800, height=800)
#    
#    #par()
#    corrplot(cor(group_i[,c("auc_all", "cor_all", "rsq_all")]), title=paste0("Virtual Species ", vs_num, " | sample size = ", sample_size_i, " | bias type = '", bias_type_i, "' | grid side length = ", grid_side_length_i, "m"), method="number")
#    dev.off()
#    
# })
# 
# 
# sapply(unique(big_df$vs), function(vs_name_i){
#    vs_i = big_df[big_df$vs == vs_name_i,]
#    
# })

#=================================================================================================================================
# 
# 
# results_500 = big_df[big_df$sample_size == 500,]
# results_1000 = big_df[big_df$sample_size == 1000,]
# results_5000 = big_df[big_df$sample_size == 5000,]
# 
# #results_agg = aggregate(. ~ train_pct + n_backgroud + bias_type + bias_exp + correction_type + grid_side_length + grid_n_per + corrected, big_df, mean)
# #results_agg$sample_size = as.factor(results_agg$sample_size)
# #results_agg$sample_size
# #big_df = results_agg
# ggplot(big_df, aes(x=bias_exp, y=rmse_val)) + geom_point(aes(color=corrected))
# 
# ggplot(big_df, aes(x=bias_exp, y=rmse_val)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=rmse_train)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=rmse_all)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=model_auc)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=all_auc)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=train_auc)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=val_auc)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=cor_all)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=cor_train)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# ggplot(big_df, aes(x=bias_exp, y=cor_val)) + 
#    geom_line(aes(color=sample_size, linetype=corrected), size=1)
# 
# results_agg = aggregate(. ~ train_pct + n_backgroud + bias_type + bias_exp + correction_type + grid_side_length + grid_n_per + corrected, big_df, mean)
# #results_agg$corrected
# ggplot(results_agg, aes(x=bias_exp, y=rmse_val)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=rmse_train)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=rmse_all)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=model_auc)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=all_auc)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=train_auc)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=val_auc)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=cor_all)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=cor_train)) + 
#    geom_line(aes(linetype=corrected), size=1)
# 
# ggplot(results_agg, aes(x=bias_exp, y=cor_val)) + 
#    geom_line(aes(linetype=corrected), size=1)

