library(ggplot2)
library(dplyr)
library(plyr)
library(corrplot)

vs_num = 8
vs_name = paste0("vs", vs_num)
include_iter_plots = FALSE
jpg_width = 1500
jpg_height = 1500

results_all = read.csv(paste0(OUTPUT_DIR, "/results_", vs_name, ".csv"))
#results_all$sample_size = as.factor(results_all$sample_size)


#get the columns that contain the output statistics - all of the data columns are next to each other, so if I just give the name of the first one and the last one then I can get all of them without having to type them all out
first_data_col_name = "auc__model"
last_data_col_name = "rsq_all"
data_col_names = names(results_all)[which(names(results_all) == first_data_col_name):which(names(results_all) == last_data_col_name)] 
#if(!include_iter_plots){
#   iter_plot_bool = grepl("iter", data_col_names)
#   data_col_names = data_col_names[!iter_plot_bool]
#}
data_col_names = c("auc_all", "cor_all", "rmse_all", "rsq_all")
nice_names = c("AUC", "Correlation", "RMSE", "R-squared")
#sapply(results_all, function(col_i){
#   return(length(unique(col_i)))
#})


varying_vars = c("sample_size", "bias_type", "grid_side_length")
group_factor = interaction(results_all[,varying_vars])
results_all$group = group_factor
group_levels = levels(results_all$group)



#level_i = group_levels[i]
#group_i = results_all[results_all$group == level_i,]

#there should only be one value for each of these
#sample_size_i = unique(group_i$sample_size)
#bias_type_i = unique(group_i$bias_type)
#grid_side_length_i = unique(group_i$grid_side_length)

group_agg = aggregate(. ~ bias_exp + corrected, group_i, mean)

#jpeg(paste0(plot_mat_dir, "/simple_", vs_name, "_samp_", sample_size_i, "_bias_type_", bias_type_i, "_grid_side_length_", grid_side_length_i,".jpg"), width = jpg_width, height=jpg_height)

n_plots_per_side = ceiling(sqrt(length(data_col_names)))
par(mfrow = c(n_plots_per_side,n_plots_per_side), cex.main=2.2, cex.lab = 1.6, cex.axis = 1.6, oma=c(0,2,4,1))
plot_list = lapply(1:length(data_col_names), function(j){
   col_name_j = data_col_names[j]
   #plot_dat = group_agg[,c("bias_exp", "corrected", col_name_j)]
   #colnames(plot_dat) = c("bias_exp", "corrected", "value")
   #plot_i = ggplot(plot_dat, aes(x=bias_exp, y=value)) + geom_line(aes(color=corrected)) + ylab(col_name_j) + ggtitle(col_name_j)
   
   yrange = c(min(group_agg[,col_name_j]), max(group_agg[,col_name_j]))
   corrected = group_agg[group_agg$corrected == TRUE,]
   uncorrected = group_agg[group_agg$corrected == FALSE,]
   plot(uncorrected[,"bias_exp"], uncorrected[,col_name_j], type="l", col="black", lwd=2, main=nice_names[j], ylab=nice_names[j], xlab="Bias level", ylim=yrange)
   lines(corrected[,"bias_exp"], corrected[,col_name_j], type="l", col="red", lwd=2)
   legend(x="bottomright", legend = c(paste0("uncorrected"), "corrected"), lty=c(1,1), lwd=c(2,2), col=c("black", "red"), inset=c(0,1), xpd=TRUE, bty="n", horiz=FALSE, cex=1.7)
   return(NULL)
})
mtext(paste0("Virtual Species ", vs_num, " | sample size = ", sample_size_i, " | bias type = '", bias_type_i, "' | grid side length = ", grid_side_length_i, "m"), cex=2.0, outer=TRUE)
dev.off()

results_all$corrected = as.factor(results_all$corrected)

col_output_dir = paste0("/Users/dfriend/Desktop/GEOG_701M_Project/col_plots/", vs_name)

for(i in 1:ncol(results_all)){
   col_name_i = names(results_all)[i]
   jpeg(paste0(col_output_dir, "/", vs_name, "_", col_name_i, ".jpg"), width=1500, height=1500)
   par(mfrow=c(5,4), oma=c(0,0,3,0), cex.main=2, cex.lab=1.5)
   plot.column.default(results_all, col_name_i)
   mtext(col_name_i, outer=TRUE, cex=2.3)
   dev.off()
}

cor_data = results_all
cor_cols = c("auc_all", "cor_all", "rsq_all")
plot_by = "bias_type"

unique_vals = unique(cor_data[,plot_by])
n_plots_side = ceiling(sqrt(length(unique_vals)))
par(mfrow=c(n_plots_side, n_plots_side), mar=c(5,4,6,2))
for(val_i in unique_vals){
   corrplot(cor(cor_data[cor_data[,plot_by] == val_i,cor_cols]), method="number", main = val_i)   
}
par(mfrow=c(1,2))
corrplot(cor(results_all[results_all$corrected == FALSE,c("auc_all", "cor_all", "rsq_all", "rmse_all")]), method="number")
corrplot(cor(results_all[results_all$corrected == TRUE,c("auc_all", "cor_all", "rsq_all", "rmse_all")]), method="number")


vs_nums = c(5,6,7,8)
vs_names = paste0("vs", vs_nums)
df_list = lapply(vs_nums, function(num_i){
   df_i = read.csv(paste0(OUTPUT_DIR, "/results_vs", num_i, ".csv"))
   df_i$vs = paste0("vs", num_i)
   return(df_i)
})
big_df = bind_rows(df_list)


cor(big_df[,cor_cols])

#by species
cor_vs = lapply(unique(big_df$vs), function(vs_name_i){
   print(vs_name_i)
   vs_i = big_df[big_df$vs == vs_name_i,]
   print(cor(vs_i[,cor_cols]))
   return(cor(vs_i[,cor_cols]))
})

#by bias type
thing = lapply(unique(big_df$bias_type), function(bias_type_i){
   print(bias_type_i)
   group_i = big_df[big_df$bias_type == bias_type_i,]
   print(cor(group_i[,cor_cols]))
   return(cor(group_i[,cor_cols]))
})

#by sample size
thing = lapply(unique(big_df$sample_size), function(sample_size_i){
   print(sample_size_i)
   group_i = big_df[big_df$sample_size == sample_size_i,]
   print(cor(group_i[,cor_cols]))
   return(cor(group_i[,cor_cols]))
})

#by grid side length
thing = lapply(unique(big_df$grid_side_length), function(grid_side_length_i){
   print(grid_side_length_i)
   group_i = big_df[big_df$grid_side_length == grid_side_length_i,]
   print(cor(group_i[,cor_cols]))
   return(cor(group_i[,cor_cols]))
})

#by bias level
thing = lapply(unique(big_df$bias_exp), function(bias_exp_i){
   print(bias_exp_i)
   group_i = big_df[big_df$bias_exp == bias_exp_i,]
   print(cor(group_i[,cor_cols]))
   return(cor(group_i[,cor_cols]))
})
