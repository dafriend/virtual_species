library(corrplot)
prjct = CRS("+init=epsg:26913")

cutoff_cor = .4


states = c("Colorado")
region_raw = usa[usa$NAME_1 %in% vs_states,]
vars_raw = bioclim_all
vars_raw = crop(vars_raw, region_raw)

region = spTransform(region_raw, prjct)
vars = projectRaster(vars_raw, crs=prjct, res=c(1000,1000))

vars_cor_raw = layerStats(vars, 'pearson', na.rm=TRUE)
vars_cor = vars_cor_raw$`pearson correlation coefficient`
corrplot(vars_cor, method="number")

cor_bool = abs(vars_cor) >= cutoff_cor
diag(cor_bool) = FALSE

cor_pairs =  c()
for(row_index in 1:nrow(cor_bool)){
   for(col_index in 1:ncol(cor_bool)){
      if(cor_bool[row_index, col_index]){
         cor_pairs = rbind(cor_pairs, cbind(rownames(cor_bool)[row_index], colnames(cor_bool)[col_index]))
      }
   }
}
cor_pairs

#given a vector, this function returns all possible combinations (of all lengths) of the elements of the vector
get_all_combns <- function(data){
   combns <-list()
   counter <- 1
   for (i in 1:length(data)){
      temp <- combn(data, i)
      for (j in 1:ncol(temp)){
         combns[[counter]] <- as.vector(temp[,j])
         counter <- counter + 1
      }
   }
   return(combns)
}


dups_bool = duplicated(
   lapply(1:nrow(cor_pairs), function(i){
      row_i <- cor_pairs[i, ]
      row_i[order(row_i)]
   }))
cor_pairs_no_dups = cor_pairs[!dups_bool,]
nrow(cor_pairs_no_dups)
cor_pairs_no_dups



all_combns = get_all_combns(rownames(cor_bool))
all_combns_lengths = sapply(all_combns, length)
all_combns = all_combns[all_combns_lengths <= 7] #already run this... longest is 6 so don't worry about the other lengths
final_list = list()
for(i in 1:length(all_combns)){
   if(i %% 10000 == 0) { print(paste0("Loop ", i, " of ", length(all_combns)))}
   combn_i = all_combns[[i]]
   has_cor_pair = c()
   for(j in 1:nrow(cor_pairs_no_dups)){
      pair_j = cor_pairs_no_dups[j,]
      has_cor_pair_j = pair_j %in% combn_i
      has_cor_pair = c(has_cor_pair, all(has_cor_pair_j))
   }
   if(!any(has_cor_pair)){
      final_list[[length(final_list) + 1]] = combn_i
   }
}
final_list
combn_lengths = sapply(final_list, length)
table(combn_lengths)
big_combns = final_list[combn_lengths == max(combn_lengths)]
big_combns


final_comb = big_combns[[3]]

final_comb
final_rasters = vars[[final_comb]]
cor2_raw = layerStats(final_rasters, "pearson", na.rm=TRUE)
cor2 = cor2_raw$`pearson correlation coefficient`
corrplot(cor2, method="number")




