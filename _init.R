library(raster)
library(rgeos)
library(virtualspecies)
library(spatstat)
#dyn.load("/Library/Java/JavaVirtualMachines/jdk-11.0.3.jdk/Contents/Home/lib/server/libjvm.dylib")
#dyn.load('/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/server/libjvm.dylib')
#Sys.setenv(JAVA_HOME = "/Library/Java/JavaVirtualMachines/jdk-11.0.3.jdk")
#Sys.setenv(JAVA_HOME = "/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk")
#Sys.setenv(JAVA_HOME = "/usr/libexec/java_home")
library(rJava)
library(dismo)
library(parallel)
library(rgdal)
library(pROC)

#BASE_DIR = "C:/Users/Derek Friend/Documents/Derek/UNR_Spring_2019/701M project/virtual_species"


BASE_DIR = "/Users/dfriend/Documents/UNR_Schoolwork/Spring 2019/GEOG_701M_Project/virtual_species" #UNR Mac Workstation
FUNCTIONS_DIR = paste0(BASE_DIR, "/functions")
DATA_DIR = paste0(BASE_DIR, "/data")
BIOCLIM_DIR = paste0(DATA_DIR, "/bioclim")
ROADS_DIR = paste0(DATA_DIR, "/roads")
US_STATES_DIR = paste0(DATA_DIR, "/states")
VIRTUAL_SPECIES_DIR = paste0(BASE_DIR, "/virtual_species")
PROB_SURFACES_DIR = paste0(DATA_DIR, "/prob_surfaces")
FOCUS_POINTS_DIR = paste0(DATA_DIR, "/vs_focus_points")
OUTPUT_DIR = paste0(BASE_DIR, "/output")
#PROJECTION = CRS("+init=epsg:26911")

setwd(BASE_DIR)

function_files = dir(FUNCTIONS_DIR)
sapply(function_files, function(file_name){
   print(paste0(FUNCTIONS_DIR, "/", file_name))
   source(paste0(FUNCTIONS_DIR, "/", file_name))
})

bioclim_all = raster::getData(name="worldclim", var="bio", res=2.5, path=BIOCLIM_DIR)
us_states = raster::getData(name="GADM", country="USA", level=1, path=US_STATES_DIR)
